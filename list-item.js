const React = require('react');
const { Transition } = require('react-transition-group');
const { TweenLite } = require('gsap');

class ListItem extends React.Component {
  constructor(props){
    super(props);
    this.handleEnter = this.handleEnter.bind(this);
    this.handleExit = this.handleExit.bind(this);
    this.handleCallback = this.handleCallback.bind(this);
    this.registerCallback = this.registerCallback.bind(this);
  }

  handleEnter(el, isAppearing) {
    TweenLite.from(el, 0.5, {
      x: -200,
      onComplete: this.handleCallback
    })
  }

  handleExit(el) {
    TweenLite.to(el, 0.5, {
      alpha: 0,
      onComplete: this.handleCallback
    })
  }

  handleCallback() {
    this.cb();
  }

  registerCallback(el, cb) {
    this.cb = cb;
  }

  render(){
    return(
      <Transition
        {...this.props}
        appear={true}
        onEnter={this.handleEnter}
        onExit={this.handleExit}
        addEndListener={(node, done) => this.registerCallback(node, done)}
      >
        <li>
          Lista
        </li>
      </Transition>
    )
  }
}

module.exports = ListItem;
