const stuff = (state = [], action) => {
  switch (action.type) {
    case 'ADD_STUFF':
      return [
        ...state,
        {
          id: action.id,
          text: action.text,
          completed: false
        }
      ];
    case 'REMOVE_STUFF':
      return state.slice(0, state.length -1);
    default:
      return state
  }
}

module.exports = stuff;