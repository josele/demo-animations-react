const React = require('react');
const ReactDom = require('react-dom');
const { connect, Provider } = require('react-redux');
const { TransitionGroup } = require('react-transition-group');
const store = require('./store');
const ListItem = require('./list-item');
const actions = require('./actions');

class App extends React.Component {
  constructor(props){
    super(props)
  }

  render(){
    return (
      <div>
        <TransitionGroup>
          { this.props.stuff.map(i => <ListItem key={i.id}/>) }
        </TransitionGroup>
        <button onClick={this.props.addStuff}>Add</button>
        { this.props.stuff.length > 0 && <button onClick={this.props.removeStuff}>Remove</button> }
      </div>
    )
  }
}

const mapStateToProps = state => {
  return {
    stuff: state
  }
};

const ConnectedApp = connect(mapStateToProps, actions)(App);

const Main = () =>
  <Provider store={store}>
    <ConnectedApp />
  </Provider>;


ReactDom.render(<Main />, document.getElementById('root'));