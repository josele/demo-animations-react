const addStuff = () => {
  return {
    type: 'ADD_STUFF',
    id: Math.random()
  }
};

const removeStuff = () => {
  return {
    type: 'REMOVE_STUFF',
  }
};


module.exports = {
  addStuff,
  removeStuff
};


